package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"strings"
	"time"
)

var client *mongo.Client

var secretKey = []byte("JWTSecretKey")

type Account struct {
	ID        	primitive.ObjectID 	`json:"_id,omitempty" bson:"_id,omitempty"`
	Email 		string             	`json:"email,omitempty" bson:"email,omitempty"`
	Password  	[]byte             	`json:"password,omitempty" bson:"password,omitempty"`
}

type Claims struct {
	Email string `json:"email"`
	jwt.StandardClaims
}

//CreateAccount
func CreateAccount(w http.ResponseWriter, r *http.Request) {
	var account Account

	r.ParseForm()
	account.Email = r.Form.Get("email")
	password := []byte(r.Form.Get("password"))
	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	account.Password = hashedPassword
	collection := client.Database("api").Collection("account")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, _ := collection.InsertOne(ctx, account)
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(result)

}

//Authenticate
func Authenticate(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	email := r.Form.Get("email")
	password := []byte(r.Form.Get("password"))

	var account Account
	collection := client.Database("api").Collection("account")
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	err := collection.FindOne(ctx,
		Account{
			Email: email,
		}).Decode(&account)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	err = bcrypt.CompareHashAndPassword(account.Password, password)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(`{ "message": "Invalid Password" }`))
	}

	token, err := GenerateJWT(email)
	if err != nil {
		fmt.Printf(err.Error())
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(token)


}
//GenerateJWT  create the JWT claims, which includes the email and expiry time
func GenerateJWT(email string) (string, error) {
	expirationTime := time.Now().Add(15 * time.Minute)

	claims := &Claims{
		Email: email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(secretKey)

	if err != nil {
		log.Println("Error is: ", err)
		return "", nil
	}

	return tokenString, nil
}

//isAuthorized middleware for JWT authentication and validation
func isAuthorized(next func(w http.ResponseWriter, r *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// get Authorization header and split it
		authorizationHeader := r.Header.Get("authorization")
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				// verify the token by the signature
				token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
					if err, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						log.Println(err)
						return nil, fmt.Errorf("internal server error")
					}
					return secretKey, nil
				})
				if token.Valid {
					next(w, r)
				} else {
					log.Println("Error is: ", err)
					w.WriteHeader(http.StatusUnauthorized)
					return
				}
			}
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
	})
}

func main() {
	fmt.Println("REST API by vavas")

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, _ = mongo.Connect(ctx, clientOptions)

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/create_account", CreateAccount).Methods("POST")
	router.HandleFunc("/authenticate", Authenticate).Methods("POST")

	//secured area
	router.Handle("/secured_method", isAuthorized(CreateAccount)).Methods("POST")
	log.Fatal(http.ListenAndServe(":8080", router))
}