# golang_test
REST API for Daxx test

# Create Account

Create Account endpoint:
`POST http://localhost.com:80/create_account`

Request params:
``` json
	
	  {
	    "email": "test@test.com",
	    "password": "testpass"
	  }
```

# Authenticate

Authenticate endpoint:
`POST http://localhost.com:80/authenticate`

Request params:

``` json
	
	  {
	    "email": "test@test.com",
	    "password": "testpass"
	  }
```



